#!/bin/bash

NUM_CPU_CORES=$(nproc --all)

cpulimit -e "biandewi" -l $((70 * $NUM_CPU_CORES))